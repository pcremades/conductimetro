/****************************************************
CONDUCTImtero - CoSensores (Sensores Comunitarios) 
*****************************************************
Adaptación del codigo de DFRobot Gravity: Analog TDS Sensor/Meter 
https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_TDS_Sensor_/_Meter_For_Arduino_SKU:_SEN0244    
*****************************************************/

#ifndef GRAVITY_TDS_H
#define GRAVITY_TDS_H

#include "Arduino.h"

#define ReceivedBufferLength 15
#define TdsFactor 0.5  //tds = ec / 2

class GravityTDS
{
public:
    GravityTDS();
    ~GravityTDS();

    void begin();  //initializatcio'n
    void update(); //leer y calcular
    void setPin(int pin); 
    void setTemperature(float temp);  //define temperatura para compensacio'n del valor de consuctividad
    void setAref(float value);  //voltage de referencia en ADC, default 5.0V en Arduino UNO
    void setAdcRange(float range);  //1024 para 10bit ADC;4096 para 12bit AD
    void setKvalueAddress(int address); //define 'Address' en EEPROM para guardar el kValue (default address:0x08)
    float getKvalue(); 
    float getTdsValue();
    float getEcValue();

private:
    int pin;
    float aref;  //default 5.0V en Arduino UNO
    float adcRange;
    float temperature;
    int kValueAddress;     //l'Address del kValue guardada en EEPROM
    char cmdReceivedBuffer[ReceivedBufferLength+1];   // guarda el dato cmd del monitor serial
    byte cmdReceivedBufferIndex;
 
    float kValue;      //valor k value del sensor calibrado 
    float analogValue;
    float voltage;
    float ecValue; //valor de coductividad antes de compensar por temperatura
    float ecValue25; //valor de conductividad despues de compensar por temperatura
    float tdsValue;

    void readKValues();
    boolean cmdSerialDataAvailable();
    byte cmdParse();
    void ecCalibration(byte mode);
};  

#endif
